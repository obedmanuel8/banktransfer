package com.maybank.banktransfer.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import com.maybank.banktransfer.helper.UserAuthenticationSuccessHandler;
import com.maybank.banktransfer.service.CustomUserDetailsService;



@Configuration
@EnableWebSecurity
public class MyAuth {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private UserAuthenticationSuccessHandler successHandler;
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(customUserDetailsService);
		authProvider.setPasswordEncoder(bCryptPasswordEncoder());
		return authProvider;
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();

	}
	
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception{
		
		httpSecurity.authorizeRequests().antMatchers("/nasabah")
			.hasAnyAuthority("cservice");
		httpSecurity.authorizeRequests().antMatchers("/provider")
			.hasAnyAuthority("admin");
		httpSecurity.authorizeRequests().antMatchers("/transfer")
			.hasAnyAuthority("operator");
		httpSecurity.csrf().disable();
		httpSecurity.authorizeRequests().antMatchers("/api/**")
			.authenticated().and().httpBasic();
//		httpSecurity.authorizeRequests().anyRequest().authenticated();
		httpSecurity.authorizeRequests().and().formLogin().successHandler(successHandler);
		
		return httpSecurity.build();
	}
}
