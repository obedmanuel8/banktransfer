package com.maybank.banktransfer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.banktransfer.entity.Provider;
import com.maybank.banktransfer.service.ProviderService;



@Controller
@RequestMapping("/provider")
public class ProviderController {
	
	@Autowired
	private ProviderService providerService;
	
	@GetMapping
	public String index(Model model) {
		List<Provider> providers = this.providerService.getAll();
		
		model.addAttribute("providerForm",new Provider());
		
		model.addAttribute("providers",providers);
		
		
		return "provider";
	}
	
	@PostMapping("/save")
	public String save(@ModelAttribute("providerForm") Provider provider, RedirectAttributes redirectAttributes) {
//		System.out.println("employee name: "+ employee.getFirstName());
		this.providerService.save(provider);
		
//		redirectAttributes.addFlashAttribute("success", "data inserted");
		return "redirect:/provider";
	}
	
	@GetMapping("/delete")
	public String delete(Provider provider,RedirectAttributes redirectAttributes) {
		this.providerService.delete(provider.getId());
		return "redirect:/provider"; 
	}
}
