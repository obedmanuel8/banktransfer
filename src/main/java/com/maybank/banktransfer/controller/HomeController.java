package com.maybank.banktransfer.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/")
public class HomeController {
	
	@GetMapping
	public String index(Model model) {
		
		String springMessage = "hello spring";
//		model.addAttribute("employees",employees);
		model.addAttribute("springMessage", springMessage);
		return "home";
		

		
		
		

	}
	
}
