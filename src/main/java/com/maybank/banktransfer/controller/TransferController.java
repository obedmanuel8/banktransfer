package com.maybank.banktransfer.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.banktransfer.entity.History;
import com.maybank.banktransfer.entity.Rekening;
import com.maybank.banktransfer.entity.Transfer;
import com.maybank.banktransfer.service.HistoryService;
import com.maybank.banktransfer.service.ProviderService;
import com.maybank.banktransfer.service.RekeningService;
import com.maybank.banktransfer.service.TransferService;

@Controller
@RequestMapping("/transfer")
public class TransferController {
	
	@Autowired
	private TransferService transferService;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private HistoryService historyService;
	
	@GetMapping
	public String index(Model model) {
		
		Page<History> listHistory;
		Page<Transfer> listTransfer;
		List<Transfer> listTransfers;
		List<Rekening> rekenings = this.rekeningService.getAll();
		
		listTransfers = this.transferService.getAll();
		model.addAttribute("transferForm", new Transfer());
		model.addAttribute("historyForm", new History());
		model.addAttribute("page", listTransfers);
		model.addAttribute("rekList", rekenings);
		
		return "transfer";
		
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("transferForm") Transfer transfer,@Valid @ModelAttribute("historyForm") History history, BindingResult result, RedirectAttributes redirectAttributes, Model model) {
		
		Transfer tf = transfer;
		
		
//		List<Rekening> reklist = this.rekeningService.getAll();
		
		if(result.hasErrors()) {
			model.addAttribute("transfer");
			return "transferTransaction";
		}else {
			Rekening rekeningPengirim = rekeningService.findByNoRekening(tf.getRekening_pengirim().getNoRekening());
			Rekening rekeningPenerima = rekeningService.findByNoRekening(tf.getRekening_penerima().getNoRekening());
			history.setRekeningPenerima(rekeningPenerima.getNoRekening());
			history.setRekeningPengirim(rekeningPengirim.getNoRekening());
			history.setTanggalKirim(Date.valueOf(LocalDate.now()));
			Long idPengirim = rekeningPengirim.getId();
			Long idPenerima = rekeningPenerima.getId();
			if(idPengirim == null || idPenerima == null) {
				model.addAttribute("transfer");
				redirectAttributes.addFlashAttribute("Gagal!", "Rekening Pengirim atau Penerima tidak ditemukan");
				return "redirect:/transfer";
			}
			else if(idPengirim!=null && idPenerima!=null) {
				Optional<Rekening> pengirim = this.rekeningService.getRekeningById(idPengirim);
				Optional<Rekening> penerima = this.rekeningService.getRekeningById(idPenerima);
				Rekening rekPengirim = pengirim.get();
				Rekening rekPenerima = penerima.get();
				String bankAsal = rekPengirim.getProvider().getNamaBank();
				String bankTuju = rekPenerima.getProvider().getNamaBank();
				double amount = tf.getAmount();
				double sisa;
				if(bankAsal.equals(bankTuju)) {
					sisa = tf.getRekening_pengirim().getSaldo() - amount;
					if(sisa < 50000) {
						model.addAttribute("transfer");
						redirectAttributes.addFlashAttribute("Fail", "Saldo anda kurang dari 50000");
						return "redirect:/transfer";
					}
					else {
						this.transferService.save(tf);
						this.historyService.save(history);
						redirectAttributes.addFlashAttribute("Success", "Transfer Success");
						return "redirect:/transfer";
					}
				}else if(bankAsal != bankTuju){
					double fee = 6500;
					sisa = tf.getRekening_pengirim().getSaldo() - amount - fee;
					if(sisa < 50000) {
						model.addAttribute("transfer");
						redirectAttributes.addFlashAttribute("Fail", "Saldo kurang dari 50000");
						return "redirect:/transfer";
					}
					else {
						this.transferService.save(tf);
						this.historyService.save(history);
						redirectAttributes.addFlashAttribute("Success", "Transfer Success");
						return "redirect:/transfer";
					}
			}
		
		
		
		
			}
		}
		return "transfer";
	}
	}
		
