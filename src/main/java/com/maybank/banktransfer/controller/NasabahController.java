package com.maybank.banktransfer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.maybank.banktransfer.entity.Nasabah;
import com.maybank.banktransfer.entity.Provider;
import com.maybank.banktransfer.entity.Rekening;
import com.maybank.banktransfer.repository.ProviderRepo;
import com.maybank.banktransfer.repository.RekeningRepo;
import com.maybank.banktransfer.service.NasabahService;
import com.maybank.banktransfer.service.ProviderService;
import com.maybank.banktransfer.service.RekeningService;

@Controller
@RequestMapping("/nasabah")
public class NasabahController {
	
	@Autowired
	private NasabahService nasabahService;
	
	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private RekeningService rekeningService;
	
	@GetMapping
	public String index(Model model) {
		
		List<Rekening> rekenings = this.rekeningService.getAll();
		List<Provider> providers = this.providerService.getAll();
		
		model.addAttribute("nasabahForm",new Nasabah());
		
		model.addAttribute("dataRekForm",new Rekening());
		
		model.addAttribute("dataReks", rekenings);
		
		model.addAttribute("providers",providers);
		
		
		return "nasabah";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("nasabahForm")Nasabah nasabah,
			@Valid @ModelAttribute("dataReks") Rekening rekening,
			@Valid @ModelAttribute("providers") Provider provider,
			Model model, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return "nasabah";
		}
		this.nasabahService.save(nasabah);
		rekening.setNasabah(nasabah);
		rekening.setProvider(provider);
		this.rekeningService.save(rekening);
		return "redirect:/nasabah";
	}
	

	@GetMapping("/delete")
	public String delete(@RequestParam("id")Long id) {
		this.rekeningService.delete(id);
		return "redirect:/nasabah";
	}
	
}
