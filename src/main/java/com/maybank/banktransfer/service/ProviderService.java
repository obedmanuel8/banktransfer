package com.maybank.banktransfer.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.maybank.banktransfer.entity.Provider;


public interface ProviderService {
	public List<Provider> getAll();
	
	public void save(Provider provider);
	
	public void delete(Long id);
}
