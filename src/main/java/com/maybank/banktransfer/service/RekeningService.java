package com.maybank.banktransfer.service;

import java.util.List;
import java.util.Optional;

import com.maybank.banktransfer.entity.Provider;
import com.maybank.banktransfer.entity.Rekening;

public interface RekeningService {
	public List<Rekening> getAll(); 
	
	public Rekening findByNoRekening(String noRekening);
	
	public void save(Rekening rekening);
	
	public void delete(Long id);
	
	public void getById(Long id);
	
	public Optional<Rekening> getRekeningById(Long id);
	
	
}
