package com.maybank.banktransfer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maybank.banktransfer.entity.Rekening;
import com.maybank.banktransfer.entity.Transfer;
import com.maybank.banktransfer.repository.TransferRepo;

@Service
@Transactional
public class TransferServiceImpl implements TransferService{
	
	
	@Autowired
	private TransferRepo transferRepo;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Override
	public List<Transfer> getAll() {
		// TODO Auto-generated method stub
		return this.transferRepo.findAll();
	}

	
		
	@Override
	public void save(Transfer transfer) {
		// TODO Auto-generated method stub
		Rekening rekPengirim = rekeningService.findByNoRekening(transfer.getRekening_pengirim().getNoRekening());
		Rekening rekPenerima = rekeningService.findByNoRekening(transfer.getRekening_penerima().getNoRekening());
		
		Long idSender =rekPengirim.getId();
		Long idReceiver = rekPenerima.getId();
		
		Optional<Rekening> sender = this.rekeningService.getRekeningById(idSender);
		Optional<Rekening> receiver = this.rekeningService.getRekeningById(idReceiver);
		
		Rekening rekSender = sender.get();
		Rekening rekReceive = receiver.get();
		
		String bankSender = rekSender.getProvider().getNamaBank();
		String bankReceiver = rekReceive.getProvider().getNamaBank();
		
		Transfer transfer2 = new Transfer();
		transfer2.setAmount(transfer.getAmount());
		
		if(bankSender.equals(bankReceiver)) {
			transfer2.setFee(0.0);
			rekSender.setSaldo(rekSender.getSaldo()-(transfer2.getAmount()));
			transfer2.setRekening_pengirim(rekSender);
			transfer2.setRekening_penerima(rekReceive);
			rekReceive.setSaldo(rekReceive.getSaldo()+(transfer2.getAmount()));
			
			this.rekeningService.save(rekSender);
			this.rekeningService.save(rekReceive);
			this.transferRepo.save(transfer2);
		}else {
			transfer2.setFee(6500.0);
			rekSender.setSaldo(rekSender.getSaldo()-(transfer2.getAmount()+6500));
			transfer2.setRekening_pengirim(rekSender);
			transfer2.setRekening_penerima(rekReceive);
			rekReceive.setSaldo(rekReceive.getSaldo()+(transfer2.getAmount()));
			
			this.rekeningService.save(rekSender);
			this.rekeningService.save(rekReceive);
			this.transferRepo.save(transfer2);
		}
	}
	
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.transferRepo.deleteById(id);
		
	}

	@Override
	public Optional<Transfer> getTransferById(Long id) {
		// TODO Auto-generated method stub
		return this.transferRepo.findById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.transferRepo.findById(id);
	}



}
