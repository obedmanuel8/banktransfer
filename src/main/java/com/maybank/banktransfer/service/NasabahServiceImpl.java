package com.maybank.banktransfer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.banktransfer.entity.Nasabah;
import com.maybank.banktransfer.repository.NasabahRepo;

@Service
public class NasabahServiceImpl implements NasabahService{
	
	@Autowired
	private NasabahRepo nasabahRepo;
	
	@Override
	public List<Nasabah> getAll() {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findAll();
	}

	@Override
	public void save(Nasabah nasabah) {
		// TODO Auto-generated method stub
		this.nasabahRepo.save(nasabah);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.nasabahRepo.deleteById(id);
	}
}
