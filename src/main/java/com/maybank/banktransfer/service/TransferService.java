package com.maybank.banktransfer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.banktransfer.entity.Transfer;

public interface TransferService {
	
//	public Page<Transfer> getPaginateSearch(int pageNo, int pageSize, String field, String keyword);
//    public Page<Transfer> getAllPaginate(int pageNo, int pageSize, String field);
    public List<Transfer> getAll();
    public void save(Transfer transfer);
    public Optional<Transfer> getTransferById(Long id);
    public void delete(Long id);
    public void getById(Long id);

}
