package com.maybank.banktransfer.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.banktransfer.entity.Role;
import com.maybank.banktransfer.entity.User;
import com.maybank.banktransfer.repository.RoleRepo;
import com.maybank.banktransfer.repository.UserRepo;

@Service
@Transactional
public class InitDefaultUserAuth {
	
	@Autowired
	private RoleRepo roleRepo;
	
	@Autowired
	private UserRepo userRepo;
	
	@PostConstruct
	public void index() {
		
//		Create Role
		Role roleOperator = new Role();
		Role roleCservice = new Role();
		Role roleAdmin = new Role();
		
		roleOperator.setRole("operator");
		roleCservice.setRole("cservice");
		roleAdmin.setRole("admin");
		
		this.roleRepo.save(roleOperator);
		this.roleRepo.save(roleCservice);
		this.roleRepo.save(roleAdmin);
		
//		Operator
		List<Role> operatorListRole = new ArrayList<>();
		operatorListRole.add(roleOperator);
		
//		Customer Service
		List<Role> csListRole = new ArrayList<>();
		csListRole.add(roleCservice);
		
//		Admin
		List<Role> adminListRole = new ArrayList<>();
		adminListRole.add(roleAdmin);
		
//		Create Operator
		User userOperator = new User();
		userOperator.setUsername("operator");
		
		userOperator.setPassword(new BCryptPasswordEncoder().encode("123456789"));
		userOperator.setRoles(operatorListRole);
	
//		Create User
		User userCservice = new User();
		userCservice.setUsername("cservice");
		
		userCservice.setPassword(new BCryptPasswordEncoder().encode("123456789"));
		userCservice.setRoles(csListRole);
		
//		Create Admin
		User userAdmin = new User();
		userAdmin.setUsername("admin");
		
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("123456789"));
		userAdmin.setRoles(adminListRole);
		
		this.userRepo.save(userOperator);
		this.userRepo.save(userAdmin);
		this.userRepo.save(userCservice);
		
	}
}
