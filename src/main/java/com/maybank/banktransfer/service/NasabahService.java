package com.maybank.banktransfer.service;

import java.util.List;

import com.maybank.banktransfer.entity.Nasabah;
import com.maybank.banktransfer.entity.Provider;


public interface NasabahService {

	public List<Nasabah> getAll();
	
	public void save(Nasabah nasabah);
	
	public void delete(Long id);
}
