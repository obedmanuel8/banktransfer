package com.maybank.banktransfer.service;

import java.util.List;
import java.util.Optional;

import com.maybank.banktransfer.entity.History;

public interface HistoryService {
	public List<History> getAll();
	public void save(History history);
	public void delete(Long id);
	public void getById(Long id);
	public Optional<History> getRekeningById(Long id);
}
