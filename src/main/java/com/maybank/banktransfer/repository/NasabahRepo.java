package com.maybank.banktransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.banktransfer.entity.Nasabah;

public interface NasabahRepo extends JpaRepository<Nasabah, Long>{
	
	
}
