package com.maybank.banktransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.banktransfer.entity.Provider;

public interface ProviderRepo extends JpaRepository<Provider, Long>{
	
}
