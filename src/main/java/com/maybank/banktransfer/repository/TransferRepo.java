package com.maybank.banktransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.banktransfer.entity.Transfer;

public interface TransferRepo extends JpaRepository<Transfer, Long>{

}
