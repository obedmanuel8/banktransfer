package com.maybank.banktransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.banktransfer.entity.History;

public interface HistoryRepo extends JpaRepository<History, Long>{

}
