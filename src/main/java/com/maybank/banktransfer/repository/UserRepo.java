package com.maybank.banktransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.banktransfer.entity.User;

public interface UserRepo extends JpaRepository<User, Long>{

	User findByUsername(String username);

}
