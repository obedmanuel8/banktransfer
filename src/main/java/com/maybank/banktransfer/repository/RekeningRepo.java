package com.maybank.banktransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.banktransfer.entity.Rekening;

public interface RekeningRepo extends JpaRepository<Rekening, Long>{
	Rekening findByNoRekening(String noRekening);
}
