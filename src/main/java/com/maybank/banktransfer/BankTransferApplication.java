package com.maybank.banktransfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.maybank.banktransfer.entity.Provider;
import com.maybank.banktransfer.repository.ProviderRepo;

@SpringBootApplication
public class BankTransferApplication implements ApplicationRunner{

	public static void main(String[] args) {
		SpringApplication.run(BankTransferApplication.class, args);
	}
	
	   @Autowired
	    private ProviderRepo providerRepo;

	    @Override
	    public void run(ApplicationArguments args) throws Exception{

	        Provider provider = new Provider();
	        provider.setNamaBank("Maybank");
	        this.providerRepo.save(provider);

	    }
}
