package com.maybank.banktransfer.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Transfer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER.LAZY)
	@JoinColumn(name="pengirim_id", referencedColumnName = "id")
	private Rekening rekening_pengirim;
	
	@ManyToOne(fetch=FetchType.EAGER.LAZY)
	@JoinColumn(name="penerima_id", referencedColumnName = "id")
	private Rekening rekening_penerima;
	

	@CreatedDate
	@Column(name="tgl_kirim")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date tanggalKirim;
	
	private Double amount;
	
	private Double fee;
	
	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Rekening getRekening_pengirim() {
		return rekening_pengirim;
	}

	public void setRekening_pengirim(Rekening rekening_pengirim) {
		this.rekening_pengirim = rekening_pengirim;
	}

	public Rekening getRekening_penerima() {
		return rekening_penerima;
	}

	public void setRekening_penerima(Rekening rekening_penerima) {
		this.rekening_penerima = rekening_penerima;
	}

	public Date getTanggalKirim() {
		return tanggalKirim;
	}

	public void setTanggalKirim(Date tanggalKirim) {
		this.tanggalKirim = tanggalKirim;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	
	
	
}
