package com.maybank.banktransfer.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Nasabah {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@NotBlank
	@NotEmpty
	@Column(name = "nama_lengkap",nullable = false)
	private String namaLengkap;
	
	private Date tgl_lahir;
	
	private String no_id;
	
	private String tipe_id;
	
	private String email;
	
	private String noContact;
	
	@OneToMany(mappedBy = "nasabah")
	private List<Rekening> rek_list;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamaLengkap() {
		return namaLengkap;
	}

	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}

	public Date getTgl_lahir() {
		return tgl_lahir;
	}

	public void setTgl_lahir(Date tgl_lahir) {
		this.tgl_lahir = tgl_lahir;
	}

	public String getNo_id() {
		return no_id;
	}

	public void setNo_id(String no_id) {
		this.no_id = no_id;
	}

	public String getTipe_id() {
		return tipe_id;
	}

	public void setTipe_id(String tipe_id) {
		this.tipe_id = tipe_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoContact() {
		return noContact;
	}

	public void setNoContact(String noContact) {
		this.noContact = noContact;
	}
	
	
	
}
